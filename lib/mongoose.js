var mongoose=require('mongoose');
var config=require('nconf');

mongoose.connect(config.get('mongoose:uri'));

module.exports=mongoose;