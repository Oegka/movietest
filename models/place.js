var mongoose = require('mongoose');
var _ = require('lodash');

var Place = mongoose.Schema({
        number: {
            type: Number,
            unique: true,
            require: true
        },
        state: {
            type: String,
            unique: false,
            require: false
        }
    }
);

Place.statics.sort = function(params,docs) {
    if (params.sortMode == 'min') {
        return Promise.resolve(_.sortBy(docs,params.sort));
    }
    else {
        return Promise.resolve(_.sortBy(docs,params.sort).reverse());
    }
};

Place.statics.limit = function(params,doc) {
    return Promise.resolve(doc.slice(params.head,parseInt(params.head) + parseInt(params.limit)));
};

module.exports = mongoose.model("Place",Place);
