var webpack = require('webpack');

module.exports = {

    context: __dirname + '/frontend',
    entry: ['./index.jsx'],

    output: {
        path: __dirname + '/public',
        filename: 'build.js'
    },
    watch: true,
    noParse: /react\/react.js/,
    plugins: [
        new webpack.ProvidePlugin({
            cloneDeep: 'lodash/lang/cloneDeep',
            fetch: 'isomorphic-fetch'
        })
    ],
    module: {
        loaders: [
            { test: /\.jsx$/, loader: 'babel', exclude: /node_modules/},
            { test: /\.js$/, loader: 'babel', exclude: /node_modules/},
            { test: /\.html$/, loader: 'html'},
            { test: /\.css$/, loader: 'style!css'}

        ]
    },

    babel: {
        presets: ['es2015','react'],
        plugins: ['transform-runtime']
    }
};