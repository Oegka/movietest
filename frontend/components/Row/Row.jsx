import React, { Component, PropTypes } from 'react'
import Place from './Place/Place.jsx'
import styles from './Row.css'

export default class Row extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const {index} = this.props
        let currentRow = this.props.amountRows - index
        return (
            <div id = 'row' className="item" style = {styles}>
                <div className="ui grid">
                    <div className="right aligned two wide column">
                        {currentRow} ряд
                    </div>
                    <div className="fourteen wide column">
                        {this.props.row.map(function(place,i) {
                            return <Place place = {place} index = {i} addPlace = {this.props.addPlace} delPlace = {this.props.delPlace}
                                checkAmount = {this.props.checkAmount} setError = {this.props.setError}/>
                        }.bind(this))
                        }
                    </div>
                </div>
            </div>

        )
    }
}