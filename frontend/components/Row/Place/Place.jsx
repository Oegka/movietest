import React, { Component, PropTypes } from 'react'
import classNames from 'classnames/bind'

export default class Place extends Component {

    constructor(props) {
        super(props)
        this.state = {
            status: this.props.place.state
        }
        this.changeStatus = this.changeStatus.bind(this)
    }
    componentWillMount() {

    }

    shouldComponentUpdate(newProps,newState) {
        //console.log(newProps.place.state != this.props.place.state)
        return newProps.place.state != this.props.place.state || newState.status != this.state.status

    }

    componentWillUpdate(newProps,newState) {

        newProps.place.state != this.props.place.state && this.setState({
            status: newProps.place.state
        })
    }

    render() {
        console.log('render')
        const {place} = this.props
        const {index} = this.props
        let classes = classNames({
            'ui': true,
            'blue': this.state.status == 'free',
            'green': this.state.status == 'selected',
            'red': this.state.status == 'busy',
            'label': true
        })
        return (
            <a className={classes} onClick = {e => this.changeStatus(place)}>
                {index + 1}
            </a>
        )
    }

    changeStatus(place) {
        switch(this.state.status) {
            case 'free':
                if (this.props.checkAmount()) {
                    this.props.addPlace(place)
                    this.setState({status: 'selected'})
                }
                break
            case 'selected':
                this.props.delPlace(place)
                this.setState({status: 'free'})
                break
            case 'busy':
                this.props.setError(true,'Место занято')
        }
    }
}


