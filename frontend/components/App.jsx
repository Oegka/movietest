import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as PageActions from '../actions/PageAction.js'
import Row from './Row/Row.jsx'
import styles from './App.css'
import classNames from 'classnames/bind'
import _ from 'lodash'

export default class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            amountRows: 20,
            selectedPlacesNow: [],
            amount: 0,
            myError: {
                now: false,
                message: ''
            }
        }
        this.sendInfoOfNewOrder = this.sendInfoOfNewOrder.bind(this);
        this.addSelectedPlace = this.addSelectedPlace.bind(this)
        this.delSelectedPlace = this.delSelectedPlace.bind(this)
        this.checkingForAmount = this.checkingForAmount.bind(this)
        this.setError = this.setError.bind(this)
    }

    componentWillMount() {
        this.props.pageActions.getPlaces(this.state.amountRows)
    }

    componentWillUpdate(newProps,newState) {
        console.log(newState.selectedPlacesNow)
    }

    render() {
        const {rows} = this.props.page
        const {fetching} = this.props.page
        let error = classNames({
            'ui': true,
            'form': true,
            'error': this.state.myError.now
        })
        let loaded = classNames({
            'ui': true,
            'list': true,
            'center': true,
            'aligned': true,
            'loading': fetching,
            'segment': true
        })

        return (
            <div id="places" style = {styles}>
                <div className="ui grid">
                    <div className="twelve wide column">
                        <div className="ui circular labels">
                            <div className={loaded}>
                                {rows.map(function(row,i) {
                                    return <Row index = {i} row = {row} amountRows = {this.state.amountRows}
                                                addPlace = {this.addSelectedPlace} delPlace = {this.delSelectedPlace}
                                                checkAmount = {this.checkingForAmount} setError = {this.setError}/>
                                }.bind(this)) }
                            </div>
                        </div>
                    </div>
                    <div id="paid" className="center aligned four wide column">
                        <div className={error}>
                            <div className="field">
                                <div className="ui error message">
                                    <div className="header">Ошибка</div>
                                    <p>{this.state.myError.message}</p>
                                </div>
                                <div className="ui submit purple basic button" onClick = {e => this.sendInfoOfNewOrder()}>Submit</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

    addSelectedPlace(place) {
        this.setState({
            selectedPlacesNow: this.state.selectedPlacesNow.concat([place])
        })
        console.log(place.number)
        this.setError(false)

    }
    delSelectedPlace(place) {
        this.setState({
            selectedPlacesNow : this.state.selectedPlacesNow.filter(function(placeNow) {
                return place.number != placeNow.number
            })
        })
        console.log(place.number)
        this.setError(false)
    }

    checkingForAmount() {
        if(this.state.selectedPlacesNow.length + 1 < 6) {
            return true
        }
        else {
            this.setError(true,'Выберите меньше 6-ти мест для брони')
            return false
        }
    }

    setError(flag,error) {
        this.setState ({
            myError: {
                now: flag,
                message: error
            }
        })
    }
    sendInfoOfNewOrder() {
        let data = this.state.selectedPlacesNow
        $.ajax({
            type: 'POST',
            url: '/addOrders',
            data: JSON.stringify({places: data})
        })
            .done(function() {
                this.props.pageActions.getPlaces(this.state.amountRows)
                this.setState({
                    selectedPlacesNow: []
                })
                this.setError(false)
            }.bind(this))
    }
}

function mapStateToProps(state) {
    return {
        page: state.page
    }
}

function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators(PageActions,dispatch)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);