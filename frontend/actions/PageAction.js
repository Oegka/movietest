export const GET_PLACES = 'GET_PLACES'
export const GET_PLACES_WAIT = 'GET_PLACES_WAIT'
export const SEND_ORDERS = 'SEND_ORDERS'
export const SEND_ORDERS_WAIT = 'SEND_ORDERS_WAIT'
const AMOUNT_PLACES_IN_SERIES = 20

export function getPlaces(amountRows) {

    return (dispatch) => {
        dispatch({
            type: GET_PLACES_WAIT
        })

        return fetch(`/getPlaces`)
            .then(function(req) {
                return req.json()
            })
            .then(function(data) {
                dispatch({
                    type: GET_PLACES,
                    places: data,
                    amountRows: amountRows
                })
            })

    }
}

