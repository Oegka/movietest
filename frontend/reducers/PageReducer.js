import _ from 'lodash'

const initialState = {
    rows: [],
    fetching: false
}

export default function page(state = initialState,action) {

    switch(action.type) {
        case 'GET_PLACES':
            return  Object.assign({}, state, {
                rows: rows(undefined,action),
                fetching: false
            })
        case 'GET_PLACES_WAIT':
            return  Object.assign({}, state, {
                rows: [],
                fetching: true
            })
        default:
            return state
    }
}

function rows(state,action) {
    switch(action.type) {
        case 'GET_PLACES':
            return  distributionSeatsOfRow(action)
        default:
            return state
    }
}

function distributionSeatsOfRow(action) {
    let arrayRows = []
    let count = 0
    while (action.places.length > count) {
        arrayRows.push(action.places.slice(count,count + action.amountRows).reverse())
        count = count + action.amountRows
    }
    return arrayRows
}