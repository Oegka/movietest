var Place = require('models/place');

function placeRepository() {

}

module.exports = placeRepository;

placeRepository.prototype.createPlaces = function(amountPlace) {
    return new Promise(function(resolve,reject) {
        var places = createArrayPlaces(amountPlace);
        Promise.all(places.map(function(placeNum) {
            var place = new Place();
            place.number = placeNum;
            place.state = 'free';
            return place.save();
        }))
            .then(function() {
                resolve();
            });
    })
};

function createArrayPlaces(amountPlace) {
    var array = [];
    for (var i = 0; i < amountPlace; i ++) {
        array[i] = i;
    }
    return array;
}

placeRepository.prototype.getPlaces = function(params) {
    return new Promise(function(resolve,reject) {

        if (!params.sort) {
            params.sort = 'number';
        }
        if (!params.sortMode) {
            params.sortMode = 'min';
        }

        Place.find()
            .then(function(docs) {
                return Place.sort(params,docs);
            })
            .then(function(docs) {
                return Place.limit(params,docs);
            })
            .then(function(docs) {
                resolve(docs);
            })
    });
};

placeRepository.prototype.addOrderPlaces = function(places) {
    console.log(places);
    return Promise.all(places.map(function(place) {
        return this.changeState(place.number,'busy');
    }.bind(this)))
}

placeRepository.prototype.findByNumber = function(number) {
    return new Promise(function(resolve,reject) {
        Place.findOne({number: number})
            .then(function(place) {
                resolve(place)
            });
    });
};

placeRepository.prototype.changeState = function(number,state) {
    return new Promise(function(resolve,reject) {
        this.findByNumber(number)
            .then(function(place) {
                place.state = state;
                return place.save();
            })
            .then(function() {
                console.log('yoyy');
                resolve();
            })
    }.bind(this))
}
