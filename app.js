var express = require('express');
var fs = require('fs');

var config=require('config');
var mongoose = require('lib/mongoose');

var app = express();

app.get('/', function(req,res) {
    sendFile('index.html',res);
});

require('./routes')(app);

app.use(express.static('./public'));


app.listen(3000, function() {
    console.log("Express server listening on port %d in %s mode");
});



function sendFile(fileName,res) {
    var fileStream = fs.createReadStream(fileName);
    fileStream.on('error',function() {
        res.statusCode = 500;
        res.end('Server error');
    });
    fileStream.on('open',function() {
        fileStream.pipe(res);
    });
}

