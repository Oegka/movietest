var Place = require('repository/placeRepository');

module.exports = function(req,res,next) {
    bodyParser(req)
        .then(function(data) {

            var place = new Place();
            return place.addOrderPlaces(data.places);
        })
        .then(function() {
            res.sendStatus(200);
        });
};

function bodyParser(req) {
    return new Promise(function(resolve,reject) {
        var body='';
        req.on('readable', function(){
            body += req.read();
        });
        req.on('end', function() {
            body = JSON.parse(body);
            resolve(body);
        });
    });
}