var placeRep = require('repository/placeRepository');

module.exports = function(req,res,next) {
    var params = {
        head: req.query.head || 0,
        limit: req.query.limit || 400,
        sortMode: 'max'
    };
    var rep = new placeRep();

    rep.getPlaces(params)
        .then(function(places) {
            res.end(JSON.stringify(places));
        });
};